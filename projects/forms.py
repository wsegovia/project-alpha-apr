from django import forms  # Importing the forms module from Django
from .models import Project  # Importing the Project model from the current app


class ProjectForm(
    forms.ModelForm
):  # Creating a form class called ProjectForm that extends forms.ModelForm
    class Meta:  # Defining the metadata for the form
        model = Project  # Setting the model for the form as Project
        fields = [  # Specifying the fields to include in the form
            "name",
            "description",
            "owner",
        ]
