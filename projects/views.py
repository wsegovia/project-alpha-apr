from django.shortcuts import render, redirect, get_object_or_404  # Importing the render, redirect, and get_object_or_404 function from Django's shortcuts module
from django.contrib.auth.decorators import login_required  # Importing the login_required decorator from Django's auth.decorators module
from .forms import ProjectForm  # Importing the ProjectForm class from the current package's forms module
from .models import Project  # Importing the Project model from the current package's models module


@login_required  # Applying the login_required decorator to the view function to protect it
def list_projects(request):  # Defining a view function named list_projects
    projects = Project.objects.filter(
        owner=request.user
    )  # Filtering the projects queryset based on the logged-in user
    context = {
        "projects": projects  # Creating a dictionary with the key "projects" and the value of the projects variable
    }
    return render(request, "projects/list_projects.html", context)
    # Using the render function to render the "projects/list_project.html" template with the given context and return the resulting HTTP response


@login_required  # Decorator to ensure that the user must be logged in to access this view
def project_detail(
    request, id
):  # View function that takes a request object and an id parameter
    project = get_object_or_404(
        Project, id=id
    )  # Retrieves the project object with the given id from the Project model or returns a 404 error page if it doesn't exist
    return render(
        request, "projects/project_detail.html", {"project": project}
    )  # Renders the project_detail.html template with the project object passed as a context variable


def project_create_view(request):  # Define a view function named project_create_view that takes a request object
    if request.method == "POST":  # Check if the HTTP request method is POST
        form = ProjectForm(request.POST)  # Create an instance of ProjectForm with the submitted POST data
        if form.is_valid():  # Check if the form data is valid
            form.save()  # Save the form data to create a new project
            return redirect("list_projects")  # Redirect the user to the list_projects view
    else:
        form = ProjectForm()  # Create an instance of ProjectForm (empty form)

    return render(request, "projects/create_project.html", {"form": form})
    # Render the create_project.html template with the form as a context variable and return the resulting HTTP response
