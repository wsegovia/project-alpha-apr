from django.urls import (
    path,
)  # Importing the path function from Django's urls module
from .views import (
    list_projects,
    project_detail,
    project_create_view,
)  # Importing the list_projects function from the views module of the projects package


urlpatterns = [
    path(
        "", list_projects, name="list_projects"
    ),  # Defining a URL pattern with an empty string path that maps to the list_projects function and assigns it the name "list_projects"
    path(
        "create/", project_create_view, name="create_project"
    ),  # Defining URL pattern for creating a new project
    path(
        "<int:id>/", project_detail, name="show_project"
    ),  # Defining a URL pattern with an integer parameter path that maps to the project_detail function and assigns it the name "show_project"
]
