from django.contrib import admin  # Importing the admin module from Django
from .models import (
    Project,
)  # Importing the Project model from the current package


@admin.register(Project)  # Registering the Project model with the admin site
class ProjectAdmin(
    admin.ModelAdmin
):  # Defining a custom ModelAdmin class named ProjectAdmin
    pass
