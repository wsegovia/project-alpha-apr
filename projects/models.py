from django.db import (
    models,
)  # Importing the models module from Django for creating models
from django.contrib.auth.models import (
    User,
)  # Importing the User model from Django's authentication system


class Project(
    models.Model
):  # Defining the Project model as a subclass of models.Model
    name = models.CharField(
        max_length=200
    )  # Defining a CharField attribute named 'name' with a maximum length of 200 characters
    description = (
        models.TextField()
    )  # Defining a TextField attribute named 'description' with no maximum length
    owner = models.ForeignKey(
        User, related_name="projects", on_delete=models.CASCADE, null=True
    )
    # Defining a ForeignKey attribute named 'owner' that references the User model
    # The related_name parameter sets the reverse relation name from User to projects
    # The on_delete parameter specifies that when the referenced User object is deleted, associated Project objects will be deleted as well
    # The null=True parameter allows the owner field to be nullable

    def __str__(
        self,
    ):  # Defining the __str__() method that returns the name of the project
        return self.name
