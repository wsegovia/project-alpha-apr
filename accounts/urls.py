from django.urls import (
    path,
)  # Importing the path function from Django's urls module
from .views import (
    login_view,
    logout_view,
    signup_view,
)  # Importing the login_view and logout_view functions from the views module of the accounts package


urlpatterns = [
    path(
        "login/", login_view, name="login"
    ),  # Defining a URL pattern for the "login/" path that maps to the login_view function and assigns it the name "login"
    path(
        "logout/", logout_view, name="logout"
    ),  # URL pattern for the "logout/" path, mapped to the logout_view function and named "logout"
    path("signup/", signup_view, name="signup"),
]
