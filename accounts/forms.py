from django import forms  # Importing the forms module from Django


class LoginForm(
    forms.Form
):  # Defining a LoginForm class that inherits from Django's forms.Form
    username = forms.CharField(
        max_length=150
    )  # Creating a CharField for the username with a max length of 150 characters
    password = forms.CharField(
        max_length=150, widget=forms.PasswordInput
    )  # Creating a CharField for the password with a max length of 150 characters and using a PasswordInput widget


class SignupForm(forms.Form):
    username = forms.CharField(
        max_length=150
    )  # CharField for username with a maximum length of 150 characters
    password = forms.CharField(
        max_length=150, widget=forms.PasswordInput
    )  # CharField for password with a maximum length of 150 characters and rendered as a password input field
    password_confirmation = forms.CharField(
        max_length=150, widget=forms.PasswordInput
    )  # CharField for password confirmation with a maximum length of 150 characters and rendered as a password input field
