from django.shortcuts import (
    render,
    redirect,
)  # Importing necessary functions from Django's shortcuts module
from .forms import (
    LoginForm,
    SignupForm,
)  # Importing the LoginForm from the forms module in the current directory
from django.contrib.auth import (
    authenticate,
    login,
    logout,
)  # Importing necessary functions for authentication
from django.contrib.auth.models import (
    User,
)  # Importing the User model from Django's contrib.auth package


def login_view(request):
    if (
        request.method == "POST"
    ):  # If the request method is POST (form submission)
        form = LoginForm(
            request.POST
        )  # Create a form instance with the submitted data
        if form.is_valid():  # Validate the form data
            username = form.cleaned_data[
                "username"
            ]  # Get the cleaned data for the username field
            password = form.cleaned_data[
                "password"
            ]  # Get the cleaned data for the password field
            user = authenticate(
                request, username=username, password=password
            )  # Authenticate the user
            if user is not None:  # If the user is authenticated successfully
                login(request, user)  # Log the user in
                return redirect(
                    "list_projects"
                )  # Redirect to the list_projects page
    else:
        form = (
            LoginForm()
        )  # If the request method is GET (initial page load), create an empty form

    return render(
        request, "accounts/login.html", {"form": form}
    )  # Render the login.html template with the form


def logout_view(request):
    logout(request)  # Logs out the user by clearing the session data
    return redirect(
        "login"
    )  # Redirects the user to the login page after logout


def signup_view(request):
    if (
        request.method == "POST"
    ):  # If the request method is POST (form submission)
        form = SignupForm(
            request.POST
        )  # Create a form instance with the submitted data
        if form.is_valid():  # Check if the form data is valid
            username = form.cleaned_data[
                "username"
            ]  # Get the cleaned data for the username field
            password = form.cleaned_data[
                "password"
            ]  # Get the cleaned data for the password field
            password_confirmation = form.cleaned_data[
                "password_confirmation"
            ]  # Get the cleaned data for the password_confirmation field

            if (
                password == password_confirmation
            ):  # Check if the password and password_confirmation match
                user = User.objects.create_user(
                    username=username, password=password
                )  # Create a new user with the provided username and password
                login(request, user)  # Log the user in
                return redirect(
                    "list_projects"
                )  # Redirect to the list_projects page
            else:
                form.add_error(
                    "password_confirmation", "The passwords do not match"
                )  # Add an error to the form indicating that the passwords do not match
    else:
        form = (
            SignupForm()
        )  # If the request method is GET, create an empty form instance

    return render(
        request, "registration/signup.html", {"form": form}
    )  # Render the signup.html template with the form
