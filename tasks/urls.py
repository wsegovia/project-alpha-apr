from django.urls import (
    path,
)  # Importing the path function from Django's urls module
from .views import (
    create_task,
    show_my_tasks,
)  # Importing the create_task view function from the current package's views module


urlpatterns = [
    path(
        "create/", create_task, name="create_task"
    ),  # Defining a URL pattern with the path "create/" that maps to the create_task view function and assigns it the name "create_task"
    path("mine/", show_my_tasks, name="show_my_tasks"),
]
