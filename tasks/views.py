from django.contrib.auth.decorators import (
    login_required,
)  # Importing the login_required decorator from Django's auth module
from django.shortcuts import (
    render,
    redirect,
)  # Importing the render and redirect functions from Django's shortcuts module
from .forms import (
    TaskForm,
)  # Importing the TaskForm from the forms module of the current package
from .models import Task


@login_required  # Applying the login_required decorator to the create_task view function
def create_task(request):
    if request.method == "POST":  # Checking if the request method is POST
        form = TaskForm(
            request.POST
        )  # Creating an instance of TaskForm with the POST data
        if form.is_valid():  # Checking if the form data is valid
            form.save()  # Saving the form data to create a new Task instance
            return redirect(
                "list_projects"
            )  # Redirecting to the "list_projects" URL pattern
    else:  # Executed if the request method is not POST
        form = TaskForm()  # Creating an empty instance of TaskForm

    context = {
        "form": form
    }  # Creating a dictionary with the form instance as the value
    return render(
        request, "tasks/create_task.html", context
    )  # Rendering the "create_task.html" template with the form data in the context


@login_required
def show_my_tasks(request):
    assigned_tasks = Task.objects.filter(assignee=request.user)

    context = {"tasks": assigned_tasks}
    return render(request, "tasks/my_tasks.html", context)
