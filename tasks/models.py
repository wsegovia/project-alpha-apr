from django.db import (
    models,
)  # Importing the models module from Django's db package
from django.contrib.auth.models import (
    User,
)  # Importing the User model from Django's auth package
from projects.models import (
    Project,
)  # Importing the Project model from the projects app


class Task(models.Model):
    name = models.CharField(
        max_length=200
    )  # Define a CharField attribute for the name of the task with a maximum length of 200 characters
    start_date = (
        models.DateTimeField()
    )  # Define a DateTimeField attribute for the start date of the task
    due_date = (
        models.DateTimeField()
    )  # Define a DateTimeField attribute for the due date of the task
    is_completed = models.BooleanField(
        default=False
    )  # Define a BooleanField attribute for the completion status of the task with a default value of False
    project = models.ForeignKey(
        Project,
        related_name="tasks",  # Define a foreign key relationship with the Project model, with the related name "tasks"
        on_delete=models.CASCADE,  # Set the on_delete behavior to CASCADE, which deletes the task if its associated project is deleted
    )
    assignee = models.ForeignKey(
        User,
        related_name="tasks",  # Define a foreign key relationship with the User model, with the related name "tasks"
        on_delete=models.CASCADE,
        null=True,  # Set the on_delete behavior to CASCADE, allowing null values for the assignee field
    )
